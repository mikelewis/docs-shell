# Test file (docs test files)
test_pull() {
  echo 'compile/pull.sh ok'
}

# ---------------------------------------------------------------- #

# Pull all repos
# Usage: docs pull (pull and housekeep all repos); docs pull [repo] [repo] ... [repo]
# Examples: docs pull gitlab; docs pull gitlab runner; docs pull omnibus gitlab shell; docs pull docs
pull() {
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      go_$arg
      if [[ $? != 0 ]]; then
        echo "Possible variations: \n $ docs pull \n $ docs pull [repo] [repo] ... [repo]"
        echo "   Options [repo]: gitlab, runner, omnibus, charts, docs, shell"
        return
      else
        tput setaf 5 ; echo "Pulling $arg" ; tput sgr0
        git_status
        git_master
        git_config_rebase
        git_pull
      fi
    done
  else
    update -h -a
  fi
  cd $DSHELL
}

# Usage: docs reset [repo]
reset() {
  if [[ $@ ]] ; then
    for arg in "$@" ; do
      tput setaf 5 ; echo "Reseting $arg..." ; tput sgr0
      go_$arg
      if [[ $? != 0 ]]; then
        error_sound
        echo "Possible variations: \n$ docs reset [repo] [repo] ... [repo]"
        echo "   Options [repo]: gitlab, runner, omnibus, charts, docs, shell"
        return
      else
        ok_sound ; tput setaf 3
        read "REPLY?Are you sure you want to reset`tput setaf 5``tput bold` $arg`tput sgr0``tput setaf 3`? All your changes `tput bold`will be lost`tput sgr0``tput setaf 3`. Continue (y/n)?"
        tput sgr0
        if [[ $REPLY =~ ^[Yy]$ ]] ; then
          tput setaf 5 ; echo "Reseting $arg" ; tput sgr0
          git_status
          git_master
          echo `tput bold`"$ git reset --hard origin/master" `tput sgr0`; git reset --hard origin/master
        else
          echo "Ok. Aborting."
        fi
      fi
    done
  else
    echo "Repos need to be reset one by one."
  fi
  cd $DSHELL
}
