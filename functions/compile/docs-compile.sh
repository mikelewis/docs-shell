# Test file (docs test files)
test_compile() {
  echo 'functions/compile/docs-compile.sh ok'
}

# ---------------------------------------------------------------- #

# Docs compile #

docs_compile(){
  DBUN=$(bundler_version)
  echo `tput setaf 5`"Checking out gitlab-docs master..." `tput sgr0`
  git_master
  echo `tput setaf 5`"Pulling master..." `tput sgr0`
  git_update_quick
  echo `tput setaf 5`"Updating gems..." `tput sgr0`
  echo `tput bold`"$ bundle "_"$DBUN"_" install --quiet" `tput sgr0`; bundle '_'$DBUN'_' install --quiet
  echo `tput setaf 5`"Compiling site..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc compile" `tput sgr0`; bundle exec nanoc compile
  ifsuccess
}

docs_live() {
  go_docs
  echo `tput setaf 5`"Running docs live preview..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; bundle exec nanoc live -p $PORT
  if [[ $? != 0 ]] ; then
    echo `tput setaf 5`"Nanoc was already running. Killing the process and starting it over..."`tput sgr0`
    echo `tput bold`"$ pkill nanoc\ live" `tput sgr0` `pkill nanoc\ live`
    echo `tput bold`"$ bundle exec nanoc live -p $PORT" `tput sgr0`; bundle exec nanoc live -p $PORT
  fi
}

docs_kill_nanoc_live(){
  echo `tput setaf 3`"Stopping docs live preview..." `tput sgr0`
  echo `tput bold`"$ pkill nanoc\ live" `tput sgr0` `pkill nanoc\ live`
}

docs_recompile(){
  echo `tput setaf 5`"Recompiling site..." `tput sgr0`
  echo `tput bold`"$ bundle exec nanoc compile" `tput sgr0`; bundle exec nanoc compile
  ifsuccess
}

### `docs` fuctions

# Usage: docs compile; docs compile live; docs compile live lint
compile() {
  go_docs
  docs_compile
  if [[ $1 == 'live' ]]; then
    if [[ $2 == 'lint' ]]; then
      docs_lint_while_live
    fi
    docs_live
  fi
}

# Usage: docs recompile; docs recompile lint; docs recompile lint live
recompile () {
  go_docs
  if [[ $1 == 'lint' ]]; then
    if [[ $2 == 'live' ]]; then
      docs_recompile_lint_live
    else
      docs_recompile_lint
    fi
  else
    docs_recompile
  fi
}

# Usage: docs live
live() {
  docs_live
}

# Usage: docs open_preview (open a new browser window or tab to see docs live preview)
open_preview() {
  open http://localhost:$PORT
}

## nanoc compile and nanoc check
# Usage: docs nanoc compile; docs nanoc live; docs nanoc check internal_links; docs nanoc check internal_anchors
nanoc () {
  go_docs
  echo `tput setaf 5`"Running: bundle exec nanoc $1 $2" `tput sgr0`; bundle exec nanoc $1 $2
}

# Usage: docs kill
kill() {
  docs_kill_nanoc_live
}
