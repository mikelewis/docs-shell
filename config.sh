#!/bin/bash

# Paths #
source paths.sh

# Variables #
source functions/assets/variables.sh

# Functions #
source functions.sh

# User variables
if [ -e functions/assets/user-variables.sh ] ; then
  source functions/assets/user-variables.sh
fi

