# Test files (docs test files)
test_paths() {
  echo 'paths.sh ok'
}

# ---------------------------------------------------------------- #

# Read the folder this repo was cloned into and set it as $LOCAL
# To check, run echo $LOCAL

_loc(){
  cd .. && pwd
}

CURRENT=$PWD

# ---------------------------------------------------------------- #

# Read current paths from .paths #

# Local
path_to_local() {
  awk 'NR==2' .paths
}

# GL
path_to_gl() {
  awk 'NR==4' .paths
}

# Shell
path_to_shell() {
  awk 'NR==6' .paths
}

# Docs
path_to_docs() {
  awk 'NR==8' .paths
}

# GitLab
path_to_gitlab() {
  awk 'NR==10' .paths
}

# Omnibus
path_to_omn() {
  awk 'NR==12' .paths
}

# Runner
path_to_run() {
  awk 'NR==14' .paths
}

# Charts
path_to_char() {
  awk 'NR==16' .paths
}

user_variables(){
  echo 'LOCAL=$(path_to_local)'
  echo 'GL=$(path_to_gl)'
  echo 'GITLAB=$(path_to_gitlab)'
  echo 'OMN=$(path_to_omn)'
  echo 'RUN=$(path_to_run)'
  echo 'CHAR=$(path_to_char)'
  echo 'DOC=$(path_to_docs)'
  echo 'DSHELL=$(path_to_shell)'
} > functions/assets/user-variables.sh

# ---------------------------------------------------------------- #

# Set custom paths `docs paths`, `docs paths --reset` (reset all to default)

paths() {
  if [ -e functions/assets/user-variables.sh ] ; then
    echo "Removing current user variables..."
    rm functions/assets/user-variables.sh
  fi
  if [ -e .paths ] ; then
    echo "Removing current paths..."
    rm .paths
  fi
  if [[ $1 == "--reset" ]]; then
    default_paths
  else
    custom_paths
  fi
  symlink_check_all
  tput setaf 3 ; echo `tput bold`"Check the paths:"`tput sgr0` ; cat .paths
  ok_sound ; tput setaf 3; read "REPLY?Do the paths match (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    user_variables
    if [[ $DGDK == '0' ]] ; then
      echo "PORT='3005'" >> functions/assets/user-variables.sh
      echo "DGDK='0'" >> functions/assets/user-variables.sh
      tput setaf 2 ; echo "GDK is set." ; tput sgr0
    else
      echo "PORT='3000'" >> functions/assets/user-variables.sh
    fi
    # Bc we deleted the file, add rvm/rbenv variable again to user-variables.sh
    echo "Adding Ruby version manager back to user variables..."
    set_rvmanager
    source $DSHELL/config.sh
    tput setaf 2 ; echo "User variables and paths set." ; tput sgr0
  else
    echo "Ok. Let's start over."
    paths
  fi
}

# For repos directly cloned
default_paths() {
  LOCAL=$(_loc)
  GL=$LOCAL/
  printf "DEFAULT \$LOCAL:\n$LOCAL\n" > .paths
  printf "DEFAULT \$GL:\n$GL\n" >> .paths

  DSHELL=$GL'docs-shell'
  printf "DEFAULT \$DSHELL (path to Docs Shell):\n$DSHELL\n" >> .paths
  DOC=$GL'gitlab-docs'
  printf "DEFAULT \$DOC (path to GitLab Docs):\n$DOC\n" >> .paths
  GITLAB=$GL'gitlab'
  printf "DEFAULT \$GITLAB (path to GitLab):\n$GITLAB\n" >> .paths
  OMN=$GL'omnibus-gitlab'
  printf "DEFAULT \$OMN (path to Omnibus):\n$OMN\n" >> .paths
  RUN=$GL'gitlab-runner'
  printf "DEFAULT \$RUN (path to Runner):\n$RUN\n" >> .paths
  CHAR=$GL'charts'
  printf "DEFAULT \$CHAR (path to Charts):\n$CHAR\n" >> .paths
  symlink_gitlab
  symlinks
  DGDK='1'
}

custom_paths() {
  LOCAL=$(_loc)
  GL=$LOCAL/
  printf "DEFAULT \$LOCAL:\n$LOCAL\n" > .paths
  printf "DEFAULT \$GL:\n$GL\n" >> .paths

  # GitLab Shell
  tput setaf 3
  ok_sound ; read "REPLY?Is `tput sgr0`$LOCAL/docs-shell`tput setaf 3` the path to your GitLab Shell repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    DSHELL=$GL'docs-shell'
    printf "DEFAULT \$DSHELL (path to Docs Shell):\n$DSHELL\n" >> .paths
  else
    echo "Enter the path to your GitLab Shell repo:"
    read CUSTOM_DSHELL
    DSHELL=$CUSTOM_DSHELL
    printf "CUSTOM \$DSHELL (path to Docs Shell):\n$DSHELL\n" >> .paths
  fi

  # GitLab Docs
  tput setaf 3
  ok_sound ; read "REPLY?Is `tput sgr0`$LOCAL/gitlab-docs`tput setaf 3` the path to your GitLab Docs repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    DOC=$GL'gitlab-docs'
    printf "DEFAULT \$DOC (path to GitLab Docs):\n$DOC\n" >> .paths
  else
    echo "Enter the path to your GitLab Docs repo:"
    read CUSTOM_DOC
    DOC=$CUSTOM_DOC
    printf "CUSTOM \$DOC (path to GitLab Docs):\n$DOC\n" >> .paths
  fi

  # GitLab
  ok_sound ; tput setaf 3 ; read "REPLY?Is `tput sgr0`$LOCAL/gitlab`tput setaf 3` the path to your GitLab repo (y/n)?" ; tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    GITLAB=$GL'gitlab'
    printf "DEFAULT \$GITLAB (path to GitLab):\n$GITLAB\n" >> .paths
    symlink_gitlab
  else
    tput setaf 3 ; read "REPLY?Is it GDK (y/n)?" ; tput sgr0
    if [[ $REPLY =~ ^[Yy]$ ]] ; then
      ok_sound ; echo "Enter the path to your GitLab (GDK) repo:"
      read CUSTOM_GITLAB
      GITLAB=$CUSTOM_GITLAB
      printf "CUSTOM \$GITLAB (GDK) (path to GitLab GDK):\n$GITLAB\n" >> .paths
      DGDK='0'
      symlink_gdk
    else
      ok_sound ; echo "Enter the path to your GitLab repo:"
      read CUSTOM_GITLAB
      GITLAB=$CUSTOM_GITLAB
      printf "CUSTOM \$GITLAB (path to GitLab):\n$GITLAB\n" >> .paths
      DGDK='1'
      symlink_gitlab
    fi
  fi

  # Omnibus
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/omnibus-gitlab`tput setaf 3` the path to your Omnibus GitLab repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    OMN=$GL'omnibus-gitlab'
    printf "DEFAULT \$OMN (path to Omnibus):\n$OMN\n" >> .paths
    symlink_omnibus
  else
    ok_sound ; echo "Enter the path to your Omnibus GitLab repo:"
    read CUSTOM_OMN
    OMN=$CUSTOM_OMN
    printf "CUSTOM \$OMN (path to Omnibus):\n$OMN\n" >> .paths
    symlink_omnibus
  fi

  # Runner
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/gitlab-runner`tput setaf 3` the path to your GitLab Runner repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    RUN=$GL'gitlab-runner'
    printf "DEFAULT \$RUN (path to Runner):\n$RUN\n" >> .paths
    symlink_runner
  else
    ok_sound ; echo "Enter the path to your GitLab Runner repo:"
    read CUSTOM_RUN
    RUN=$CUSTOM_RUN
    printf "CUSTOM \$RUN (path to Runner):\n$RUN\n" >> .paths
    symlink_runner
  fi

  # Charts
  tput setaf 3
  ok_sound ; read "REPLY?Is it `tput sgr0`$LOCAL/charts`tput setaf 3` the path to your GitLab Charts repo (y/n)?"
  tput sgr0
  if [[ $REPLY =~ ^[Yy]$ ]] ; then
    CHAR=$GL'charts'
    printf "DEFAULT \$CHAR (path to GitLab Charts):\n$CHAR\n" >> .paths
    symlink_charts
  else
    ok_sound ; echo "Enter the path to your GitLab Charts repo:"
    read CUSTOM_CHAR
    CHAR=$CUSTOM_CHAR
    printf "CUSTOM \$CHAR (path to GitLab Charts):\n$CHAR\n" >> .paths
    symlink_charts
  fi
}
